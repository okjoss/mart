package com.okjoss.mart.service;

import com.okjoss.mart.model.Login;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */

@RestController
@RequestMapping("/api/v1/logins")
public class LoginService extends BaseService<Login> {
}
