package com.okjoss.mart.service;

import com.okjoss.mart.model.Login;
import com.okjoss.mart.model.ResponseModel;
import com.okjoss.mart.repo.ILoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Created by Kerisnarendra on 1/07/2019.
 */

@RestController
@RequestMapping("/api/v1/auth/")
public class AuthService {
    @Autowired
    ILoginRepository repository;

    ResponseModel<Login> responseModel;

    public AuthService() {
    }

    @GetMapping("logout")
    public ResponseEntity logout(@RequestHeader("token") String token) {
        responseModel = new ResponseModel();
        responseModel.setError(false);
        responseModel.setErrorMessage("");

        Login loginResult = repository.findByToken(token);
        if (loginResult != null) {
            try {
                loginResult.setToken(null);
                Login updatedLogin = repository.save(loginResult);
            }
            catch (Exception e) {
                responseModel.setError(true);
                responseModel.setErrorMessage(e.getMessage());
            }
        }
        else {
            responseModel.setError(true);
            responseModel.setErrorMessage("Logout failed.");
        }
        return ResponseEntity.ok(responseModel);
    }

    @PostMapping("register")
    public ResponseEntity register(@RequestBody Login entity) {
        Example<Login> loginData = Example.of(entity);
        responseModel = new ResponseModel();
        responseModel.setError(false);
        responseModel.setErrorMessage("");

        // validation
        Login loginResult = repository.findByUsername(entity.getUsername()).orElse(null);
        if (loginResult == null) {
            try {
                Login createdLogin = repository.insert(entity);
                responseModel.setData(createdLogin);
            }
            catch (Exception e) {
                responseModel.setError(true);
                responseModel.setErrorMessage(e.getMessage());
            }
        }
        else {
            responseModel.setError(true);
            responseModel.setErrorMessage("Username is already taken.");
        }
        return ResponseEntity.ok(responseModel);
    }

    @PostMapping("login")
    public ResponseEntity login(@RequestBody Login entity) {
        Example<Login> loginData = Example.of(entity);
        responseModel = new ResponseModel();
        responseModel.setError(false);
        responseModel.setErrorMessage("");

        Login loginResult = repository.findOne(loginData).orElse(null);
        if (loginResult != null) {
            try
            {
                if (loginResult.getToken() == null) {
                    loginResult.setToken(UUID.randomUUID().toString());
                    Login updatedLogin = repository.save(loginResult);
                    if (!updatedLogin.getToken().isEmpty()) {
                        responseModel.setData(loginResult);
                    }
                    else {
                        responseModel.setError(true);
                        responseModel.setErrorMessage("Token could not be generated.");
                    }
                }
                else {
                    responseModel.setData(loginResult);
                }
            }
            catch (Exception e) {
                responseModel.setError(true);
                responseModel.setErrorMessage(e.getMessage());
            }
        } else if (loginResult == null) {
            responseModel.setError(true);
            responseModel.setErrorMessage("Username or password are incorrect.");
        }

        return ResponseEntity.ok(responseModel);
    }

}
