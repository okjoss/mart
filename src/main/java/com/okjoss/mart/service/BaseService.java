package com.okjoss.mart.service;

import com.okjoss.mart.model.BaseModel;
import com.okjoss.mart.repo.IBaseRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */

public class BaseService<T extends BaseModel> {
    @Autowired
    private IBaseRepository<T> repository;

    @GetMapping
    public List<T> findAll() {
        return repository.findAll();
    }

    @PostMapping
    public T create(@RequestBody T entity) {
        return repository.save(entity);
    }

    @PutMapping("/{id}")
    public T update(@PathVariable(value = "id") ObjectId id, @RequestBody T entity) {
        return repository.save(entity);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") ObjectId id) {
        repository.deleteById(id);
    }

    @GetMapping("/{id}")
    public Optional<T> get(@PathVariable(value = "id") ObjectId id) {
        return repository.findById(id);
    }
}
