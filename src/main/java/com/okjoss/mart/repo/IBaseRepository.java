package com.okjoss.mart.repo;

import com.okjoss.mart.model.BaseModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */
@NoRepositoryBean
public interface IBaseRepository<T extends BaseModel> extends MongoRepository<T, ObjectId> {
}
