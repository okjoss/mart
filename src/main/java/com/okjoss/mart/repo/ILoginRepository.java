package com.okjoss.mart.repo;

import com.okjoss.mart.model.Login;

import java.util.Optional;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */
public interface ILoginRepository extends IBaseRepository<Login> {
    Optional<Login> findByUsername(String username);
    Login findByToken(String token);
}
