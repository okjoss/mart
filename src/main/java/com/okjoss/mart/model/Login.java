package com.okjoss.mart.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */

@Getter
@Setter
@Document
public class Login extends BaseModel {
    private String username;
    private String password;
    private String token;
}
