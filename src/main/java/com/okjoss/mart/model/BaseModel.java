package com.okjoss.mart.model;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Kerisnarendra on 28/06/2019.
 */

@Getter
@Setter
@Document
public class BaseModel {
    @Id
    private ObjectId id;
}
