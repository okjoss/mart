package com.okjoss.mart.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Kerisnarendra on 2/07/2019.
 */

@Getter
@Setter
public class ResponseModel<T> {
    private boolean error;
    private String errorMessage;
    private T data;
}
