package com.okjoss.mart;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Kerisnarendra on 3/07/2019.
 * Cucumber features/scenarios are run in Alphabetical order by feature file name.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources/register.feature",
        "src/test/resources/login.feature",
        "src/test/resources/register.feature"}, glue="com/okjoss/mart")
public class CucumberTest {

}
