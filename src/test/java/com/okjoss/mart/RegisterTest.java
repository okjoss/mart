package com.okjoss.mart;

import com.github.javafaker.Faker;
import com.okjoss.mart.model.Login;
import com.okjoss.mart.model.ResponseModel;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import org.junit.Assert;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;

/**
 * Created by Kerisnarendra on 5/07/2019.
 */
public class RegisterTest extends MartApplicationTests{
    ResponseEntity<ResponseModel> responseEntity;
    Login params;
    Faker faker = new Faker();

    @When("^the client calls /api/v(\\d+)/auth/register$")
    public void the_client_calls_api_v_auth_register(int arg1) throws Throwable {
        // initiate current username and password
        setCurrentUsername(faker.name().firstName());
        setCurrentPassword(faker.crypto().md5());

        params = new Login();
        params.setUsername(getCurrentUsername());
        params.setPassword(getCurrentPassword());
        responseEntity = post("auth/register", params);
    }

    @Then("^the client receives false error code$")
    public void the_client_receives_false_error_code() throws Throwable {
        Gson gson = new Gson();
        String jsonString = new Gson().toJson(responseEntity.getBody(), ResponseModel.class);
        ResponseModel responseModel = gson.fromJson(jsonString, ResponseModel.class);

        Assert.assertEquals(responseModel.isError(), false);
    }

    @Then("^the client receives account details$")
    public void the_client_receives_account_details() throws Throwable {
        Gson gson = new Gson();
        String jsonString = new Gson().toJson(responseEntity.getBody().getData(), LinkedHashMap.class);
        Login login = gson.fromJson(jsonString, Login.class);

        Assert.assertEquals(login.getUsername().toString(), getCurrentUsername());
    }
}
