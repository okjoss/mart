package com.okjoss.mart;

import com.okjoss.mart.model.Login;
import com.okjoss.mart.model.ResponseModel;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import org.junit.Assert;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;

/**
 * Created by Kerisnarendra on 4/07/2019.
 */
public class LogoutTest extends MartApplicationTests {
    ResponseEntity<ResponseModel> loginResponseEntity;
    ResponseEntity<ResponseModel> logoutResponseEntity;
    Login params;
    String token;

    @Given("^the client calls /api/v(\\d+)/auth/login and get valid token$")
    public void the_client_calls_api_v_auth_login_and_get_valid_token(int arg1) throws Throwable {
        params = new Login();
        params.setUsername("admin");
        params.setPassword("password");
        loginResponseEntity = post("auth/login", params);

        Gson gson = new Gson();
        String jsonString = new Gson().toJson(loginResponseEntity.getBody().getData(), LinkedHashMap.class);
        Login login = gson.fromJson(jsonString, Login.class);
        setCurrentToken(token);
        token = login.getToken();
    }

    @When("^the client calls /api/v(\\d+)/auth/logout$")
    public void the_client_calls_api_v_auth_logout(int arg1) throws Throwable {
        logoutResponseEntity = getWithToken("auth/logout", token);
    }

    @Then("^the client receives false error message$")
    public void the_client_receives_false_error_message() throws Throwable {
        Gson gson = new Gson();
        String jsonString = new Gson().toJson(logoutResponseEntity.getBody(), ResponseModel.class);
        ResponseModel responseModel = gson.fromJson(jsonString, ResponseModel.class);

        Assert.assertEquals(responseModel.isError(), false);
    }
}
