package com.okjoss.mart;

import com.github.javafaker.Faker;
import com.okjoss.mart.model.Login;
import com.okjoss.mart.model.ResponseModel;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.Gson;
import org.junit.Assert;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;

/**
 * Created by Kerisnarendra on 3/07/2019.
 */
public class LoginTest extends MartApplicationTests{
    ResponseEntity<ResponseModel> responseEntity;
    Login params;

    @When("^the client calls /api/v(\\d+)/auth/login$")
    public void the_client_calls_api_v_auth_login(int arg1) throws Throwable {
        params = new Login();
        params.setUsername(getCurrentUsername());
        params.setPassword(getCurrentPassword());
        responseEntity = post("auth/login", params);
    }

    @Then("^the client receives status code of (\\d+)$")
    public void the_client_receives_status_code_of(int arg1) throws Throwable {
        Assert.assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
    }

    @Then("^the client receives login details including token$")
    public void the_client_receives_login_details_including_token() throws Throwable {
        Gson gson = new Gson();
        String jsonString = new Gson().toJson(responseEntity.getBody().getData(), LinkedHashMap.class);
        Login login = gson.fromJson(jsonString, Login.class);
        setCurrentToken(login.getToken());

        Assert.assertNotNull(login.getToken());
    }

    @When("^the client calls /api/v(\\d+)/auth/login with invalid account$")
    public void the_client_calls_api_v_auth_login_with_invalid_account(int arg1) throws Throwable {
        Faker faker = new Faker();
        params = new Login();
        params.setUsername(faker.name().username());
        params.setPassword(faker.crypto().md5());
        responseEntity = post("auth/login", params);
    }

    @Then("^the client receives true error code$")
    public void the_client_receives_true_error_code() throws Throwable {
        Gson gson = new Gson();
        String jsonString = new Gson().toJson(responseEntity.getBody(), ResponseModel.class);
        ResponseModel responseModel = gson.fromJson(jsonString, ResponseModel.class);

        Assert.assertEquals(responseModel.isError(), true);
    }
}
