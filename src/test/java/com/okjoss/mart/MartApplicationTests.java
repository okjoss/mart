package com.okjoss.mart;

import com.okjoss.mart.model.BaseModel;
import com.okjoss.mart.model.ResponseModel;
import lombok.Getter;
import lombok.Setter;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class MartApplicationTests {
	private final String BASE_API_URL = "http://localhost:8080/api/v1/";
	private RestTemplate restTemplate;
	private HttpHeaders headers;

	@Getter
	@Setter
	private String currentUsername;

	@Getter
	@Setter
	private String currentPassword;

	@Getter
	@Setter
	private String currentToken;

	public MartApplicationTests() {
		this.restTemplate = new RestTemplate();
		this.headers = new HttpHeaders();
	}

	ResponseEntity post(final String endpoint, BaseModel params) {
		return restTemplate.postForEntity(BASE_API_URL + endpoint, params, ResponseModel.class);
	}

	ResponseEntity getWithToken(final String endpoint, final String token) {
		headers.set("token", token);

		final HttpEntity<String> entity = new HttpEntity<String>(headers);
		return restTemplate.exchange(BASE_API_URL + endpoint, HttpMethod.GET, entity, ResponseModel.class);
	}
}
