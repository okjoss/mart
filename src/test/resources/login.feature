Feature: login
  Scenario: client makes call to POST /api/v1/auth/login
    When the client calls /api/v1/auth/login
    Then the client receives status code of 200
    And the client receives login details including token

  Scenario: client makes call to POST /api/v1/auth/login
    When the client calls /api/v1/auth/login with invalid account
    Then the client receives true error code