Feature: logout
  Scenario: client makes call to GET /api/v1/auth/logout including token in header
    Given the client calls /api/v1/auth/login and get valid token
    When the client calls /api/v1/auth/logout
    Then the client receives false error message